//
//  Service.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2018-01-07.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation
import CoreData

struct Service {
    static let shared = Service()
    
    let urlString = "https://api.letsbuildthatapp.com/intermediate_training/companies"
    
    func downloadCompaniesFromServer() {
        print("attempting to download")
        
        guard let url = URL(string: urlString) else { return }
//      UIApplication.shared.isNetworkActivityIndicatorVisible = true

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Failed to download companies:", error.localizedDescription)
                return
            }
            
            guard let data = data else { return }
            let jsonDecoder = JSONDecoder()
            do {
                let jsonCompanies = try jsonDecoder.decode([JSONCompany].self, from: data)
                let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                privateContext.parent = CoreDataManager.shared.persistentContainer.viewContext
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                
                jsonCompanies.forEach({ (jsonCompany) in
                    let company = Company(context: privateContext)
                    company.name = jsonCompany.name
                    let foundedDate = dateFormatter.date(from: jsonCompany.founded)
                    company.founded = foundedDate
                    
                    do {
                        try privateContext.save()
                        try privateContext.parent?.save()
                    } catch let err {
                        print("Failed to save company on privateContext:", err)
                    }
                    
                    jsonCompany.employees?.forEach({ (jsonEmployee) in
//                        print("  \(jsonEmployee.name)")
                        let employee = Employee(context: privateContext)
                        employee.fullName = jsonEmployee.name
                        employee.type = jsonEmployee.type
                        
                        let employeeInformation = EmployeeInformation(context: privateContext)
                        let birthdayDate = dateFormatter.date(from: jsonEmployee.birthday)
                        
                        employeeInformation.birthday = birthdayDate
                        employee.employeeInformation = employeeInformation
                        
                        employee.company = company
                        
                        do {
                            try privateContext.save()
                            try privateContext.parent?.save()
                        } catch let err {
                            print("Failed to save employee on privateContext:", err)
                        }
                    })
                })
            } catch let err {
                print("Failed to decode:", err)
            }
            
        }.resume() // please do not forget to make this call man
    }
}

struct JSONCompany: Decodable {
    let name: String
    let founded: String
    var employees: [JSONEmployee]?
}

struct JSONEmployee: Decodable {
    let name: String
    let type: String
    let birthday: String
}
