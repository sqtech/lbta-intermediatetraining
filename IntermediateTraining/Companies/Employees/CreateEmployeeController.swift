//
//  CreateEmployeeController.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-24.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import UIKit

protocol CreateEmployeeControllerDelegate: class {
    func didAddEmployee(employee: Employee)
}

class CreateEmployeeController: UIViewController {
    
    // MARK: Properties
    
    var company: Company?
    weak var delegate: CreateEmployeeControllerDelegate?
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        
        // Enable autolayout
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkBlue
        //        label.backgroundColor = .blue
        label.text = "Name"
        return label
    }()
    
    private let nameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        textField.textColor = .darkBlue
        textField.placeholder = "Enter Name"
        return textField
    }()
    
    private let birthdayLabel: UILabel = {
        let label = UILabel()
        
        // Enable autolayout
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkBlue
        label.text = "Birthday"
        return label
    }()
    
    private let birthdayTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        textField.textColor = .darkBlue
        textField.placeholder = "MM/DD/YYYY"
        return textField
    }()
    
    private let employeeTypeSegmentedControl: UISegmentedControl = {
//        let types = ["Executive", "Senior Management", "Staff"]
        let types = [
            EmployeeType.Executive.rawValue,
            EmployeeType.SeniorManagement.rawValue,
            EmployeeType.Staff.rawValue,
            EmployeeType.Intern.rawValue
        ]
        let sc = UISegmentedControl(items: types)
        sc.translatesAutoresizingMaskIntoConstraints = false
        sc.selectedSegmentIndex = 0
        sc.tintColor = UIColor.darkBlue
        return sc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        title = "Create Employee"
        view.backgroundColor = .darkBlue
        
        setupCancelButton()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleSave))
        
        let lightBlueBackground = setupLightBlueBackgroundView(height: 150)
        
        lightBlueBackground.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 16).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: lightBlueBackground.leadingAnchor, constant: 16.0).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        
        lightBlueBackground.addSubview(nameTextField)
        nameTextField.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor).isActive = true
        nameTextField.leadingAnchor.constraint(equalTo: nameLabel.trailingAnchor).isActive = true
        nameTextField.trailingAnchor.constraint(equalTo: lightBlueBackground.trailingAnchor, constant: -16).isActive = true
        
        lightBlueBackground.addSubview(birthdayLabel)
        birthdayLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 16).isActive = true
        birthdayLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor, constant: 0).isActive = true
        birthdayLabel.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        
        lightBlueBackground.addSubview(birthdayTextField)
        birthdayTextField.centerYAnchor.constraint(equalTo: birthdayLabel.centerYAnchor).isActive = true
        birthdayTextField.leadingAnchor.constraint(equalTo: birthdayLabel.trailingAnchor).isActive = true
        birthdayTextField.trailingAnchor.constraint(equalTo: lightBlueBackground.trailingAnchor, constant: -16).isActive = true
        
        lightBlueBackground.addSubview(employeeTypeSegmentedControl)
        employeeTypeSegmentedControl.topAnchor.constraint(equalTo: birthdayLabel.bottomAnchor, constant: 16).isActive = true
        employeeTypeSegmentedControl.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor).isActive = true
        employeeTypeSegmentedControl.trailingAnchor.constraint(equalTo: nameTextField.trailingAnchor).isActive = true
    }
    
    private func showError(title: String, with message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        return
    }
    
    @objc private func handleSave() {
        print("Handle Save")
        guard let company = company,
            let employeeName = nameTextField.text,
            let birthdayText = birthdayTextField.text else { return }
        
        if birthdayText.isEmpty {
            showError(title: "Oops", with: "Please enter a birthday")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        guard let birthdayDate = dateFormatter.date(from: birthdayText) else {
            showError(title: "Oops", with: "Please enter a valid birthday")
            return
        }
        
        guard let employeeType = employeeTypeSegmentedControl.titleForSegment(at: employeeTypeSegmentedControl.selectedSegmentIndex) else { return }
        
        CoreDataManager.shared.createEmployee(employeeName: employeeName, with: birthdayDate, of: employeeType, from: company) { employee, err in
            guard let employee = employee, err == nil else {
                print("Failed to create employee:", err?.localizedDescription ?? "")
                return
            }
            dismiss(animated: true) {
                self.delegate?.didAddEmployee(employee: employee)
            }
        }
        // Alternative way
//        let newEmployee = CoreDataManager.shared.createEmployee(employeeName: employeeName)
//        newEmployee.
    }
    

}
