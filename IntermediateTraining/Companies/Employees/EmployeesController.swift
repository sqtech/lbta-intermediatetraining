//
//  EmployeesController.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-24.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import UIKit
import CoreData

class indentedLabel: UILabel {
    override func drawText(in rect: CGRect) {
        let customRect = UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0))
        super.drawText(in: customRect)
    }
}

class EmployeesController: UITableViewController {
    
    // MARK: Properties
    
    var company: Company? {
        didSet {
            title = company?.name
        }
    }
    
    var allEmployees = [[Employee]]()
    
    var employeeTypes = [
        EmployeeType.Executive.rawValue,
        EmployeeType.SeniorManagement.rawValue,
        EmployeeType.Staff.rawValue,
        EmployeeType.Intern.rawValue
    ]
    
    struct CellIdentifiers {
        static let employeeCell = "EmployeeCell"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchEmployees()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifiers.employeeCell)
        tableView.backgroundColor = .darkBlue
        tableView.tableFooterView = UIView()
        setupPlusButtonInRightNavBar(selector: #selector(handleAdd))
    }
    
    // MARK: Private Methods
    
    private func fetchEmployees() {
        guard let companyEmployees = company?.employees?.allObjects as? [Employee] else { return }
        allEmployees = []
        
        employeeTypes.forEach { (employeeType) in
            allEmployees.append(
                companyEmployees.filter { $0.type == employeeType }
            )
        }

//        let executives = companyEmployees.filter { $0.type == EmployeeType.Executive.rawValue }
//        let seniorManagement = companyEmployees.filter { $0.type == EmployeeType.SeniorManagement.rawValue }
//        let staff = companyEmployees.filter { $0.type == EmployeeType.Staff.rawValue }
//        allEmployees = [executives, seniorManagement, staff]
        

//        employees = companyEmployees
//
//        shortNameEmployees = companyEmployees.filter({ (employee) -> Bool in
//            if let count = employee.name?.count {
//                return count <= 6
//            }
//            return false
//        })
//
//        longNameEmployees = companyEmployees.filter({ (employee) -> Bool in
//            if let count = employee.name?.count {
//                return count > 6
//            }
//            return false
//        })
//
        
        
        // Fetches all employees
//        let context = CoreDataManager.shared.persistentContainer.viewContext
//        let request = NSFetchRequest<Employee>(entityName: "Employee")
//
//        do {
//            let employees = try context.fetch(request)
//            self.employees = employees
////            employees.forEach{ print("Employee Name:", $0.name ?? "") }
//        } catch let err {
//            print("Failed to fetch employees:", err)
//        }
    }
    
    @objc private func handleAdd() {
        print("Trying to add an employee")
        let createEmployeeController = CreateEmployeeController()
        createEmployeeController.delegate = self
        createEmployeeController.company = company
        
        let navController = UINavigationController(rootViewController: createEmployeeController)
        present(navController, animated: true, completion: nil)
    }
    
    // MARK: UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return allEmployees.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allEmployees[section].count
//        return employees.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.employeeCell, for: indexPath)
        
//        let employee = employees[indexPath.row]
        
        let employee = allEmployees[indexPath.section][indexPath.row]
        
        cell.textLabel?.text = employee.fullName
        
//        if let taxId = employee.employeeInformation?.taxId {
//            cell.textLabel?.text = "\(employee.name ?? ""): \(taxId) "
//        }
        
        if let birthday = employee.employeeInformation?.birthday {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            cell.textLabel?.text = "\(employee.fullName ?? "") - \(dateFormatter.string(from: birthday))"
        }
        
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        cell.backgroundColor = UIColor.teal
        return cell
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let subclassedLabel = indentedLabel()
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let view = UIView()
        view.backgroundColor = .lightBlue
        
//        if section == 0 {
//            label.text = EmployeeType.Executive.rawValue
//        } else if section == 1 {
//            label.text = EmployeeType.SeniorManagement.rawValue
//        } else if section == 2 {
//            label.text = EmployeeType.Staff.rawValue
//        }
        
        label.text = employeeTypes[section]
        
        label.backgroundColor = UIColor.lightBlue
        label.textColor = UIColor.darkBlue
        label.font = UIFont.boldSystemFont(ofSize: 16)
        view.addSubview(label)
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
}

extension EmployeesController: CreateEmployeeControllerDelegate {
    func didAddEmployee(employee: Employee) {
//        fetchEmployees()
//        tableView.reloadData()
//        employees.append(employee)
        guard let type = employee.type, let section = employeeTypes.index(of: type) else { return }
        
        let row = allEmployees[section].count
        allEmployees[section].append(employee)
        
        let newIndexPath = IndexPath(row: row, section: section)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
    }
}
