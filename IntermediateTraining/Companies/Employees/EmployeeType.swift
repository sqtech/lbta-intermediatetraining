//
//  EmployeeType.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2018-01-03.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

enum EmployeeType: String {
    case Executive
    case SeniorManagement = "Senior Management"
    case Staff
    case Intern
}
