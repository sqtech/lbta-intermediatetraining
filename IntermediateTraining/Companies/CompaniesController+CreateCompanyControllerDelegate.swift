//
//  CompaniesController+CreateCompanyControllerDelegate.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-24.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import UIKit

// MARK: CreateCompanyControllerDelegate
extension CompaniesController: CreateCompanyControllerDelegate {
    func didAddCompany(company: Company) {
        companies.append(company)
        let newIndexPath = IndexPath(row: companies.count-1, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
    }
    
    func didEditCompany(company: Company) {
        guard let row = companies.index(of: company) else { return }
        
        let indexPathToReload = IndexPath(row: row, section: 0)
        tableView.reloadRows(at: [indexPathToReload], with: .automatic)
    }
}
