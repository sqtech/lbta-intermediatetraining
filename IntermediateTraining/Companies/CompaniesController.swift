//
//  CompaniesController.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-16.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import UIKit
import CoreData

class CompaniesController: UITableViewController {
    
    var companies = [Company]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Because of lifecycle methods, tableview will be reloaded at the end of viewDidLoad
        self.companies = CoreDataManager.shared.fetchCompanies()
        
        view.backgroundColor = .white
        navigationItem.title = "Companies"
        
        tableView.backgroundColor = .darkBlue
        tableView.separatorColor = .white
        
        tableView.register(CompanyCell.self, forCellReuseIdentifier: "cellId")
        
        // Removes the empty space below the tableView if there are not enough cells to cover the view
        tableView.tableFooterView = UIView()
        
        navigationItem.leftBarButtonItems = [
            UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(handleReset)),
            UIBarButtonItem(title: "Do Nested", style: .plain, target: self, action: #selector(doNestedUpdates)),
            UIBarButtonItem(title: "Do Updates", style: .plain, target: self, action: #selector(doUpdates)),
            UIBarButtonItem(title: "Do Work", style: .plain, target: self, action: #selector(doWork))
        ]
        
        setupPlusButtonInRightNavBar(selector: #selector(handleAddCompany))
    }
    
    @objc private func doNestedUpdates() {
        print("nested")
        
        DispatchQueue.global(qos: .background).async {
            let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateContext.parent = CoreDataManager.shared.persistentContainer.viewContext
            
            // execute updates on privateContext now
            let request: NSFetchRequest<Company> = Company.fetchRequest()
            request.fetchLimit = 1
            do {
                let companies = try privateContext.fetch(request)
                companies.forEach({ (company) in
                    print("\(company.name ?? "")")
                    company.name = "B: \(company.name ?? "")"
                })
                
                do {
                    try privateContext.save()
                    
                    DispatchQueue.main.async {
                        // Need to save on mainContext as well after saving on child privateContext (above)
                        do {
                            let context = CoreDataManager.shared.persistentContainer.viewContext
                            if context.hasChanges {
                                try context.save()
                            }
                            
                        } catch let err {
                            print("Failed to save on mainContext:", err)
                        }
                        self.tableView.reloadData()
                    }
                } catch let err {
                    print("Failed to save on privateContext:", err)
                }
                
            } catch let err {
                print("Failed to fetch on privateContext:", err)
            }
        }
    }
    
    @objc private func doUpdates() {
        CoreDataManager.shared.persistentContainer.performBackgroundTask { (backgroundContext) in
//            NSFetchRequest<Company>(entityName: "Company")
//            let company = Company()
//            company.name
            let request: NSFetchRequest<Company> = Company.fetchRequest()
            
            do {
                let companies = try backgroundContext.fetch(request)
                companies.forEach({ (company) in
                    print(company.name ?? "")
                    company.name = "A: \(company.name ?? "")"
                })
                do {
                    try backgroundContext.save()
                    DispatchQueue.main.async {
                        // Use this because the context in main thread has no idea that the background context has updated the datas
                        // expensive
                        CoreDataManager.shared.persistentContainer.viewContext.reset()
                        
                        // Don't want to refetch everything if only updating a few companies
                        self.companies = CoreDataManager.shared.fetchCompanies()
                        self.tableView.reloadData()
                    }
                } catch let err {
                    print("Failed to save on background:", err)
                }
                
                
            } catch let err {
                print("Failed to fetch companies on background:", err)
            }
        }
    }
    
    @objc private func doWork() {
        // Grand Central Dispatch - GCD
        // Background threads
//        let context = CoreDataManager.shared.persistentContainer.viewContext
//        DispatchQueue.global(qos: .background).async {}
//        CoreDataManager.shared.persistentContainer.newBackgroundContext()
        CoreDataManager.shared.persistentContainer.performBackgroundTask({ (backgroundContext) in
            //            NSEntityDescription.insertNewObject(forEntityName: "Company", into: context)
            
            (0...5).forEach({ (value) in
                print(value)
                let company = Company(context: backgroundContext)
                company.name = String(value)
            })
            
            do {
                try backgroundContext.save()
                DispatchQueue.main.async {
                    self.companies = CoreDataManager.shared.fetchCompanies()
                    self.tableView.reloadData()
                }
            } catch let err {
                print("Failed to save", err)
            }
        })
        
    }
    
    @objc private func handleReset() {
        CoreDataManager.shared.handleReset { (error) in
            guard error == nil else { return }
            
            var indexPathsToRemove = [IndexPath]()
            self.companies.enumerated().forEach({ (index, _) in
                let indexPath = IndexPath(row: index, section: 0)
                indexPathsToRemove.append(indexPath)
            })
            
            self.companies.removeAll()
            self.tableView.deleteRows(at: indexPathsToRemove, with: UITableViewRowAnimation.left)
        }
    }
    @objc private func handleAddCompany() {
        let createCompanyController = CreateCompanyController()
        let navController = CustomNavigationController(rootViewController: createCompanyController)
        createCompanyController.delegate = self
        
        present(navController, animated: true, completion: nil)
    }
}
