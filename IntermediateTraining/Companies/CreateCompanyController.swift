//
//  CreateCompanyController.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-17.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import UIKit
import CoreData

protocol CreateCompanyControllerDelegate: class {
    func didAddCompany(company: Company)
    func didEditCompany(company: Company)
}
class CreateCompanyController: UIViewController {
    
    // MARK: Properties
    weak var delegate: CreateCompanyControllerDelegate?
    
    var company: Company? {
        didSet {
            nameTextField.text = company?.name
            
            if let imageData = company?.imageData {
                companyImageView.image = UIImage(data: imageData)
                setupCircularImage()
            }
            guard let founded = company?.founded else { return }
            datePicker.date = founded
        }
    }
    
    // By declaring lazy var, self is accessible
    private lazy var companyImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "select_photo_empty"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.backgroundColor = .yellow
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true // remember to do this, otherwise image views by default are not interactive
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectorPhoto)))
        return imageView
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        
        // Enable autolayout
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkBlue
//        label.backgroundColor = .blue
        label.text = "Name"
        return label
    }()
    
    private let nameTextField: UITextField = {
       let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        textField.textColor = .darkBlue
        textField.placeholder = "Enter Name"
        return textField
    }()
    
    private let datePicker: UIDatePicker = {
       let dp = UIDatePicker()
        dp.translatesAutoresizingMaskIntoConstraints = false
        dp.datePickerMode = .date
        return dp
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = company == nil ? "Create Company" :"Edit Company"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .darkBlue
        setupCancelButton()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleSave))
        
        setupUI()
    }
    
    // MARK: Methods
    private func setupUI() {
        let lightBlueBackground = setupLightBlueBackgroundView()
        
        lightBlueBackground.addSubview(companyImageView)
        companyImageView.topAnchor.constraint(equalTo: lightBlueBackground.topAnchor, constant: 16).isActive = true
        companyImageView.centerXAnchor.constraint(equalTo: lightBlueBackground.centerXAnchor).isActive = true
        companyImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        companyImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        lightBlueBackground.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: companyImageView.bottomAnchor, constant: 16).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: lightBlueBackground.leadingAnchor, constant: 16.0).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        
        lightBlueBackground.addSubview(nameTextField)
        nameTextField.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor).isActive = true
//        nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor).isActive = true
//        nameTextField.bottomAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        nameTextField.leadingAnchor.constraint(equalTo: nameLabel.trailingAnchor).isActive = true
        nameTextField.trailingAnchor.constraint(equalTo: lightBlueBackground.trailingAnchor).isActive = true
        
        lightBlueBackground.addSubview(datePicker)
        datePicker.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        datePicker.leadingAnchor.constraint(equalTo: lightBlueBackground.leadingAnchor).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: lightBlueBackground.trailingAnchor).isActive = true
        datePicker.bottomAnchor.constraint(equalTo: lightBlueBackground.bottomAnchor).isActive = true
    }
    
    private func createCompany() {
        guard let name = self.nameTextField.text else { return }
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        let company = NSEntityDescription.insertNewObject(forEntityName: "Company", into: context)
        company.setValue(name, forKey: "name")
        company.setValue(datePicker.date, forKey: "founded")
        
        if let companyImage = companyImageView.image {
            // Convert the image to binary data
            let imageData = UIImageJPEGRepresentation(companyImage, 0.8)
            company.setValue(imageData, forKey: "imageData")
        }
        
        // perform the save
        do {
            try context.save()
            dismiss(animated: true) {
                self.delegate?.didAddCompany(company: company as! Company)
            }
        } catch let saveErr {
            print("Failed to save company:", saveErr)
        }
    }
    
    private func saveCompanyChanges() {
        let context = CoreDataManager.shared.persistentContainer.viewContext
        
        company?.name = nameTextField.text
        company?.founded = datePicker.date
        if let companyImage = companyImageView.image {
            // Convert the image to binary data
            let imageData = UIImageJPEGRepresentation(companyImage, 0.8)
            company?.imageData = imageData
        }
        
        
        do {
            try context.save()
            dismiss(animated: true, completion: {
                self.delegate?.didEditCompany(company: self.company!)
            })
        } catch let saveErr {
            print("Failed to save company changes:", saveErr)
        }
    }
    
    private func setupCircularImage() {
        companyImageView.layer.cornerRadius = companyImageView.frame.width / 2
        companyImageView.clipsToBounds = true
        companyImageView.layer.borderWidth = 1.0
        companyImageView.layer.borderColor = UIColor.darkBlue.cgColor
    }
    
    // MARK: Handlers
    @objc private func handleSave() {
        if company == nil {
            createCompany()
        } else {
            saveCompanyChanges()
        }
    }
    
    @objc private func handleSelectorPhoto() {
        print("Handle selector photo")
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        present(imagePickerController, animated: true, completion: nil)
    }
}

// MARK: UIImagePickerControllerDelegate
extension CreateCompanyController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            companyImageView.image = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            companyImageView.image = originalImage
        }
        
        setupCircularImage()
        
        dismiss(animated: true, completion: nil)
    }
}
