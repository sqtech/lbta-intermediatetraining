//
//  UIColor+theme.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-16.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import UIKit

extension UIColor {
    static let teal = UIColor(red: 48/255, green: 164/255, blue: 182/255, alpha: 1)
    static let lightRed = UIColor(red: 247/255, green: 66/255, blue: 82/255, alpha: 1.0)
    static let darkBlue = UIColor(red: 9/255, green: 45/255, blue: 64/255, alpha: 1)
    static let lightBlue = UIColor(red: 218/255, green: 235/255, blue: 243/255, alpha: 1)
}
