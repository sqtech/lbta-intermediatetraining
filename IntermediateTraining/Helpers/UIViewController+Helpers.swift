//
//  UIViewController+Helpers.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-24.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import UIKit

extension UIViewController {
    func setupPlusButtonInRightNavBar(selector: Selector) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus"), style: .plain, target: self, action: selector)
    }
    
    func setupCancelButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancelModal))
    }
    
    @objc private func handleCancelModal() {
        dismiss(animated: true, completion: nil)
    }
    
    func setupLightBlueBackgroundView(height: CGFloat = 370.0) -> UIView {
        let lightBlueBackground = UIView()
        lightBlueBackground.backgroundColor = .lightBlue
        lightBlueBackground.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(lightBlueBackground)
        NSLayoutConstraint.activate([
            lightBlueBackground.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            lightBlueBackground.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            lightBlueBackground.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            lightBlueBackground.heightAnchor.constraint(equalToConstant: height)
            ])
        
        return lightBlueBackground
    }
}
