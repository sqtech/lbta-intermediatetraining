//
//  CoreDataManager.swift
//  IntermediateTraining
//
//  Created by Simon Quach on 2017-12-17.
//  Copyright © 2017 Simon Quach. All rights reserved.
//

import CoreData

struct CoreDataManager {
    static let shared = CoreDataManager()
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "IntermediateTrainingModels")
        container.loadPersistentStores { (storeDescription, err) in
            if let err = err {
                fatalError("Loading of store failed: \(err)")
            }
        }
        return container
    }()
    
    func fetchCompanies() -> [Company] {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Company>(entityName: "Company")
        
        do {
            let companies = try context.fetch(fetchRequest)
            return companies
        } catch let fetchErr {
            print("Failed to fetch companies: ", fetchErr)
            return []
        }
    }
    
    func handleReset(completion: (_ error: String?) -> ()) {
        let context = persistentContainer.viewContext
        
//         Deleting each company one at a time, runs O(n)
//        companies.forEach { (company) in
//            context.delete(company)
////            company.name
//        }
        
        // Better way - batch delete
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Company.fetchRequest())
        
        do {
            try context.execute(batchDeleteRequest)
            completion(nil)
        } catch let delErr {
            print("Failed to delete objects from Core Data:", delErr)
            completion(delErr.localizedDescription)
        }
    }
    
    func createEmployee(employeeName: String, with birthday: Date, of type: String, from company: Company, completion: (_ employee: Employee?, _ error: Error?) -> ()) {
        let context = persistentContainer.viewContext
        let employee = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context) as! Employee
        employee.setValue(employeeName, forKey: "name")
        employee.setValue(company, forKey: "company")
        employee.setValue(type, forKey: "type")

//        let company = Company(context: context)
//        company.employees
        
        let employeeInformation = NSEntityDescription.insertNewObject(forEntityName: "EmployeeInformation", into: context) as! EmployeeInformation
        
        employeeInformation.taxId = "456"
        employeeInformation.birthday = birthday
//        employeeInformation.setValue("456", forKey: "taxId")
        
        employee.employeeInformation = employeeInformation
        
        do {
            try context.save()
            completion(employee, nil)
        } catch let err {
            completion(nil, err)
        }
    }

    // Using Tuples as return, similar to above
//    func createEmployee(employeeName: String) -> (employee: Employee?, error: Error?) {
//        let context = persistentContainer.viewContext
//        let employee = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context) as! Employee
//        employee.setValue(employeeName, forKey: "name")
//
//        do {
//            try context.save()
////            return (employee, nil)
//            return (employee: employee, error: nil)
//        } catch let err {
//            return (employee: nil, error: err)
//        }
//    }
    
    
}
